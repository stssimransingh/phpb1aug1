<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php 
        if(isset($_COOKIE['login'])){
            $login = $_COOKIE['login'];
            $username = $_COOKIE['username'];
            if($login=='true'){
                $_SESSION['login'] = true;
                $_SESSION['username'] = $username;
                header('Location: dash.php');
            }
        }

        if(isset($_REQUEST['username'])){
            if($_REQUEST['username'] == $_REQUEST['password']){
                echo "LOGIN ";
                $_SESSION['login'] = true;
                $_SESSION['username'] = $_REQUEST['username'];
                if(isset($_REQUEST['rememberme'])){
                    echo "Remember";
                    setcookie('login','true',time() + 86400);
                    setcookie('username',$_REQUEST['username'],time() + 86400);
                }else{
                    echo "Not";
                }
                header('Location: dash.php');
            }else{
                echo "Invalid";
                $_SESSION['login'] = false;
            }
        }
    ?>
    <form action="" method="post">
        <input type="text" name="username" placeholder="User Name" />
        <input type="password" name="password" placeholder="Password" />
        <input type="checkbox" name="rememberme" />Remember Me
        <input type="submit" />
    </form>
</body>
</html>