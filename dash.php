<?php
session_start();
if(isset($_SESSION['login'])){
    if($_SESSION['login']==false){
        exit('You are not authorized');
    }
}else{  
    exit('You need to login first');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>My Dashboard For <?php echo $_SESSION['username']; ?></h1>
    <a href="logout.php">Logout</a>
</body>
</html>